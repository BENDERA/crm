from django.contrib import admin
from .models import Company, Project, CompanyComment
from ckeditor.widgets import CKEditorWidget
from django import forms


class CompanyAdminForm(forms.ModelForm):
    content = forms.CharField(label= 'Описание компании', widget=CKEditorWidget())
    class Meta:
        model = Company
        fields = '__all__'

@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'fio', 'content', 'published', 'done', 'adress', 'phone')
    list_display_links = ('name', 'content')
    search_fields = ('name', 'content')
    form = CompanyAdminForm

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('company', 'title', 'content', 'date1', 'date2', 'price')
    list_display_links = ('title', 'content')
    search_fields = ('title', 'content')

@admin.register(CompanyComment)
class CompanyCommentAdmin(admin.ModelAdmin):
    list_display = ('company', 'project', 'author', 'manager', 'text', 'chanel', 'ergebnis', 'status')
    list_display_links = ('company', 'project')
    search_fields = ('company', 'text')

# @admin.register(Profile)
# class ProfileAdmin(admin.ModelAdmin):
#     list_display = ('user',)
#     list_display_links = ('user', )
#     search_fields = ('user', )


# Register your models here.
