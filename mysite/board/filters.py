import django_filters
from .models import Company, CompanyComment

class ProjectFilter(django_filters.FilterSet):

    CHOICES=(
        ('ascending', 'Самые старые'),
        ('descending', 'Самые новые')
    )
    CHOICESS = (
        ('alphabet', 'А-Я'),
        ('alphabett', 'Я-А')
    )


    ordering = django_filters.ChoiceFilter(label='Отсортировать по дате публикации', choices=CHOICES, method='filter_by_ordering')
    orderingg = django_filters.ChoiceFilter(label='Отсортировать по названию', choices=CHOICESS, method='filter_by_orderingg')

    class Meta:
        model = Company
        fields = {'name': ['icontains'],
                  'content': ['icontains'],
                  }

    def filter_by_ordering(self, queryset, name, value):
        expression = 'published' if value == 'ascending' else '-published'
        return queryset.order_by(expression)
    def filter_by_orderingg(self, queryset, name, value):
        expression = 'name' if value == 'alphabet' else '-name'
        return queryset.order_by(expression)


class CommentFilter(django_filters.FilterSet):

    class Meta:
        model = CompanyComment
        fields = {'company',
                  'project',
                  }


