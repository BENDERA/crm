from . import views
from django.urls import path, include

app_name = 'board'
urlpatterns = [
    path('', views.IndexListView.as_view(), name='index'),
    path('<int:pk>/', views.Detail.as_view(), name='detail'),
    path('companymanager/', views.Companymanager.as_view(), name='companymanager'),
    path('updatecompany/<int:pk>/', views.UpdateCompany.as_view(), name='updatecompany'),
    path('deletecompany/<int:pk>/', views.DeleteCompany.as_view(), name='deletecompany'),
    path('<int:pk>/deletecompany', views.DeleteCompany.as_view(), name='deletecompany'),
    path('<int:pk>/updateproject', views.updateproject, name='updateproject'),
    path('<int:pk>/updatecomment', views.updatecomment, name='updatecomment'),
    path('<int:pk>/updatecompany', views.UpdateCompany.as_view(), name='updatecompany'),
    path('<int:pk>/deleteproject', views.deleteproject, name='deleteproject'),
    path('<int:pk>/deletecomment', views.deletecomment, name='deletecomment'),
    path('<int:pk>/detailproject', views.DetailProject.as_view(), name='detailproject'),
    path('login', views.Login.as_view(), name='login'),
    path('register', views.Register.as_view(), name='register'),
    path('logout', views.Logout.as_view(), name='logout'),
    path('cabinet/<int:pk>/', views.Cabinet.as_view(), name='cabinet'),
    path('updatecabinet/<int:pk>', views.UpdateCabinet.as_view(), name='updatecabinet'),
    path('comment', views.Comment.as_view(), name='comment'),
    ]