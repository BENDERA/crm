from django.shortcuts import render, redirect
from .forms import CompanyForm, ProjectForm, AuthForm,  CommentForm, RegisterForm, RegisterFormm
from django.urls import reverse, reverse_lazy
from .models import Company, Project, CompanyComment
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.views.generic.edit import FormMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from .filters import ProjectFilter, CommentFilter
from django.core.paginator import Paginator


class IndexListView(ListView):
    model = Company
    template_name = 'board/index.html'
    context_object_name = 'bbs'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        filter = ProjectFilter(self.request.GET, queryset=Company.objects.all())
        paginated = Paginator(filter.qs, 2)
        page = self.request.GET.get('page')
        page_obj = paginated.get_page(page)
        context['filter'] = filter
        context['page_obj'] = page_obj
        return context

class Detail(FormMixin, DetailView):
    template_name = 'board/detail.html'
    model = Company
    form_class = ProjectForm
    second_form_class = CommentForm

    def get_success_url(self, **kwargs):
        pk = self.kwargs['pk']
        return reverse_lazy('board:detail', kwargs={'pk': pk})
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class()
        if 'form2' not in context:
            context['form2'] = self.second_form_class()
        context['current_project'] = Project.objects.filter(company = self.kwargs['pk'])
        context['current_comment'] = CompanyComment.objects.filter(company=self.kwargs['pk'])
        return context
    def post(self, request, *args, **kwargs):
        self.object = self.get_object( )
        if 'form' in request.POST:
            form_class = self.get_form_class()
            form_name = 'form'
        else:
            form_class = self.second_form_class
            form_name = 'form2'
            self.mess_type = False

        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.company=self.get_object()
        self.object.author = self.request.user
        self.object.save()
        return super().form_valid(form)


class Companymanager(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('board:login')
    model = Company
    template_name = 'board/companymanager.html'
    form_class = CompanyForm
    success_url = reverse_lazy('board:companymanager')
    def get_context_data(self, **kwargs):
        kwargs['bbs'] = Company.objects.order_by('-published')
        return super().get_context_data(**kwargs)
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.author=self.request.user
        self.object.save()
        return super().form_valid(form)


class UpdateCompany(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('board:login')
    model = Company
    template_name = 'board/companymanager.html'
    form_class = CompanyForm
    def get_success_url(self, **kwargs):
        pk = self.kwargs['pk']
        return reverse_lazy('board:detail', kwargs={'pk': pk})
    def get_context_data(self, **kwargs):
        kwargs['update'] = True,
        kwargs['bbs'] = Company.objects.order_by('-published')
        return super().get_context_data(**kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.user != kwargs['instance'].author and self.request.user.id!=1:
            return self.handle_no_permission()
        return kwargs


class DeleteCompany(LoginRequiredMixin, DeleteView):
    login_url = reverse_lazy('board:login')
    model = Company
    template_name = 'board/companymanager.html'
    success_url = reverse_lazy('board:index')
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.author != self.request.user and self.request.user.is_superuser == False and self.request.user.is_staff == False:
            return self.handle_no_permission()
        success_url = self.get_success_url()
        self.object.delete()
        return HttpResponseRedirect(success_url)

def deleteproject(request, pk):
    get_project = Project.objects.get(id=pk)
    get_project.delete()
    return redirect(reverse('board:index'))

def deletecomment(request, pk):
    get_comment = CompanyComment.objects.get(id=pk)
    get_comment.delete()
    return redirect(reverse('board:index'))

def updatecomment(request, pk):
    success_update = False
    get_comment = CompanyComment.objects.get(id=pk)
    if request.method == 'POST':
        form = CommentForm(request.POST, instance=get_comment)
        if form.is_valid():
            form.save()
            success_update = True
    template = 'board/detailproject.html'
    context = {'get_project': get_comment,
               'update': True,
               'bbs': Company.objects.order_by('-published'),
               'form': CommentForm(instance=get_comment),
               'success_update': success_update

               }
    return render(request, template, context)

def updateproject(request, pk):
    success_update = False
    get_project = Project.objects.get(pk=pk)

    if request.method == 'POST':
        form = ProjectForm(request.POST, instance=get_project)
        if form.is_valid():
            form.save()
            success_update = True
    template = 'board/detail.html'
    context = {'get_project': get_project,
               'update': True,
               'bbs': Company.objects.order_by('-published'),
               'form': ProjectForm(instance=get_project),
               'success_update': success_update

               }
    return render(request, template, context)

class Login(LoginView):
    template_name = 'board/authorisation.html'
    form_class =AuthForm
    success_url = reverse_lazy('board:index')

    def get_context_data(self, **kwargs):
        kwargs['bbs'] = Company.objects.order_by('-published')
        return super().get_context_data(**kwargs)

    def get_success_url(self):
        return self.success_url

class Register(CreateView):
    model = User
    template_name = 'board/register.html'
    form_class = RegisterForm
    success_url = reverse_lazy('board:index')
    success_msg = 'Пользователь создан'

    def get_context_data(self, **kwargs):
        kwargs['bbs'] = Company.objects.order_by('-published')
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        form_valid = super().form_valid(form)
        username =  form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        auth_user = authenticate(username=username, password=password)
        login(self.request, auth_user)
        return form_valid

class Logout(LogoutView):
    next_page = reverse_lazy('board:login')

class Cabinet(ListView):
    model = User
    template_name = 'board/cabinet.html'
    context_object_name = 'bbs'
    def get_context_data(self, **kwargs):
        kwargs['profile'] = User.objects.get(id=self.request.user.id)
        return super().get_context_data(**kwargs)


class Comment(ListView):
    model = CompanyComment
    template_name = 'board/comment.html'
    context_object_name = 'bbs'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        filter = CommentFilter(self.request.GET, queryset=CompanyComment.objects.all())
        context['filter'] = filter
        paginated = Paginator(filter.qs, 2)
        page = self.request.GET.get('page')
        page_obj = paginated.get_page(page)
        context['filter'] = filter
        context['page_obj'] = page_obj
        return context

class UpdateCabinet(UpdateView):
    model = User
    template_name = 'board/cabinet.html'
    form_class = RegisterFormm
    success_url = reverse_lazy('board:index')
    def get_context_data(self, **kwargs):
        kwargs['update'] = True,
        kwargs['profile'] = User.objects.get(pk=self.kwargs['pk'])
        return super().get_context_data(**kwargs)

class DetailProject(CreateView):
    model = Project
    template_name = 'board/detailproject.html'
    form_class = CommentForm
    def get_success_url(self, **kwargs):
        pk = self.kwargs['pk']
        return reverse_lazy('board:detailproject', kwargs={'pk': pk})
    def get_context_data(self, **kwargs):
        kwargs['project'] = Project.objects.get(pk=self.kwargs['pk'])
        kwargs['current_comment'] = CompanyComment.objects.filter(project=self.kwargs['pk'])
        return super().get_context_data(**kwargs)
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.author=self.request.user
        self.object.project = self.get_object()
        self.object.save()
        return super().form_valid(form)

